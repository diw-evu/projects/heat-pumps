# Wärmepumpen statt Erdgasheizungen: Umstieg durch Ausbau der Solarenergie unterstützen

Dieses Repositorium enthält Code und Daten, die der Analyse des Wochenberichts 22 / 2022 des DIW Berlin zugrunde liegen. Zudem enthält es weitere Abbildungen und Erklärungen.

This repository contains code and data underlying the analysis of DIW Berlin's Weekly Report 22 / 2022. In addition, it contains further figures and explanations.

Dateien / Files:

* `ResultsAnalysis.ipynb`: Enthält Abbildungen und Modelresultate / Contains figures and model results.
* `dieter_heat_module.pdf`: Vereinfachte Darstellung des Wärmemoduls im Modell DIETER / Simplied figure of the heat module in DIETER.
* `costs_online_final.xlsx`: Tabelle mit Gesamtkostenrechnung / Spreadsheet containing general cost calcutions.