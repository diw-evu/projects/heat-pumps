con5a_minRES_1c_nohp(n)$(phi_min_res(n)>0)..

          sum( h , G_L(n,'bio',h) 
        + sum( map_n_tech(n,nondis) , G_RES(n,nondis,h)) 
        + sum( map_n_rsvr(n,rsvr)   , RSVR_OUT(n,rsvr,h))

%reserves%$ontext
        - sum( reserves_do , (sum( map_n_tech(n,nondis) , RP_NONDIS(n,reserves_do,nondis,h)) + sum( map_n_rsvr(n,rsvr) , RP_RSVR(n,reserves_do,rsvr,h))) * phi_reserves_call(n,reserves_do,h))
        + sum( reserves_up , (sum( map_n_tech(n,nondis) , RP_NONDIS(n,reserves_up,nondis,h)) + sum( map_n_rsvr(n,rsvr) , RP_RSVR(n,reserves_up,rsvr,h))) * phi_reserves_call(n,reserves_up,h))
$ontext
$offtext

%prosumage%$ontext
        + sum( map_n_sto_pro(n,sto) , STO_OUT_PRO2PRO(n,sto,h) + STO_OUT_PRO2M(n,sto,h)) + sum( map_n_res_pro(n,res) , G_MARKET_PRO2M(n,res,h) + G_RES_PRO(n,res,h))
$ontext
$offtext
        )       
         
         =E= 
            
          phi_min_res(n) * sum( h , d(n,h))

        + sum( (sto,h), STO_IN(n,sto,h) - STO_OUT(n,sto,h))

%heat%$ontext

*        + sum( (bu,hp,h) , H_HP_IN(n,bu,hp,h) )

*        + sum( (bu,ch,h),                          H_STO_IN_HP(n,bu,ch,h) 
*                        + theta_elec(n,bu,ch)    * H_STO_IN_ELECTRIC(n,bu,ch,h)
*                        + theta_fossil(n,bu,ch)  * H_STO_IN_FOSSIL(n,bu,ch,h)
*                        - theta_storage(n,bu,ch) * H_STO_OUT(n,bu,ch,h)
*                        - theta_storage(n,bu,ch) * H_DHW_STO_OUT(n,bu,ch,h)
*             )
*
*        + phi_min_res(n) * sum( (bu,hp,h) , H_HP_IN(n,bu,hp,h) )

* oder:     +sum( (n, bu, hst, h), eta_heat_stat(n,bu,hst)*H_STO_LEV(n,bu,hst,h))

$ontext
$offtext

%EV%$ontext
%EV_EXOG%   + sum( (n,ev,h), EV_CHARGE(n,ev,h) - EV_GED(n,ev,h)/eta_ev_in(n,ev) - EV_DISCHARGE(n,ev,h) - (EV_L(n,ev,h)$(ord(h)=card(h)) - EV_L(n,ev,h)$(ord(h)=1)) )
$ontext
$offtext

%hydrogen_simple%$ontext

            +   phi_min_res(n) * sum( h, H2_ELEC_IN(n,h))

$ontext
$offtext

;